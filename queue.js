let collection = [];

// Write the queue functions below.

function enqueue(element) {
    collection.push(element);
    return collection;
}

function dequeue() {
    collection.shift();
    return collection;
}

function print(element) {
    return collection;
}

function front(element) {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty(element) {
    return collection.length==0;
}

module.exports = {
	enqueue,
	dequeue,
	print,
	front,
	size,
	isEmpty
};